---
- name: Install postgresql module dependency
  ansible.builtin.package:
    name: 'python{{ (ansible_distribution_major_version > "7") | ternary("3", "") }}-psycopg2'
    state: 'present'
  when: ansible_distribution in ('CentOS', 'Rocky')
  register: res
  until: res is success
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
  become: true
- name: Add or remove PostgreSQL databases
  community.general.postgresql_db:
    ca_cert: '{{ item.ca_cert | default(omit) }}'
    conn_limit: '{{ item.conn_limit | default(omit) }}'
    dump_extra_args: '{{ item.dump_extra_args | default(omit) }}'
    encoding: '{{ item.encoding | default(omit) }}'
    force: '{{ item.force | default(omit) }}'
    lc_collate: '{{ item.lc_collate | default(omit) }}'
    lc_ctype: '{{ item.lc_ctype | default(omit) }}'
    login_host: '{{ item.login_host | default(omit) }}'
    login_password: '{{ item.login_password | default(omit) }}'
    login_unix_socket: '{{ item.login_unix_socket | default(omit) }}'
    login_user: '{{ item.login_user | default(omit) }}'
    maintenance_db: '{{ item.maintenance_db | default(omit) }}'
    name: '{{ item.name | default(omit) }}'
    owner: '{{ item.owner | default(omit) }}'
    port: '{{ item.port | default(omit) }}'
    session_role: '{{ item.session_role | default(omit) }}'
    ssl_mode: '{{ item.ssl_mode | default(omit) }}'
    state: '{{ item.state | default(omit) }}'
    tablespace: '{{ item.tablespace | default(omit) }}'
    target: '{{ item.target | default(omit) }}'
    target_opts: '{{ item.target_opts | default(omit) }}'
    template: '{{ item.template | default(omit) }}'
    trust_input: '{{ item.trust_input | default(omit) }}'
  with_items: '{{ postgresql.databases | default([]) }}'
  when: postgresql is defined
  register: res
  retries: '{{ postgresql_retries }}'
  delay: '{{ postgresql_delay }}'
  become: true
  no_log: true
- name: Add or remove PostgreSQL users
  community.general.postgresql_user:
    ca_cert: '{{ item.ca_cert | default(omit) }}'
    comment: '{{ item.comment | default(omit) }}'
    conn_limit: '{{ item.conn_limit | default(omit) }}'
    db: '{{ item.db | default(omit) }}'
    encrypted: '{{ item.encrypted | default(omit) }}'
    expires: '{{ item.expires | default(omit) }}'
    fail_on_user: '{{ item.fail_on_user | default(omit) }}'
    groups: '{{ item.groups | default(omit) }}'
    login_host: '{{ item.login_host | default(omit) }}'
    login_password: '{{ item.login_password | default(omit) }}'
    login_unix_socket: '{{ item.login_unix_socket | default(omit) }}'
    login_user: '{{ item.login_user | default(omit) }}'
    name: '{{ item.name | default(omit) }}'
    no_password_changes: '{{ item.no_password_changes | default(omit) }}'
    password: '{{ item.password | default(omit) }}'
    port: '{{ item.port | default(omit) }}'
    priv: '{{ item.priv | default(omit) }}'
    role_attr_flags: '{{ item.role_attr_flags | default(omit) }}'
    session_role: '{{ item.session_role | default(omit) }}'
    ssl_mode: '{{ item.ssl_mode | default(omit) }}'
    state: '{{ item.state | default(omit) }}'
    trust_input: '{{ item.trust_input | default(omit) }}'
  with_items: '{{ postgresql.users | default([]) }}'
  when: postgresql is defined
  register: res
  retries: '{{ postgresql_retries }}'
  delay: '{{ postgresql_delay }}'
  become: true
  no_log: true
- name: Add or remove PostgreSQL extensions
  community.general.postgresql_ext:
    ca_cert: '{{ item.ca_cert | default(omit) }}'
    cascade: '{{ item.cascade | default(omit) }}'
    db: '{{ item.db | default(omit) }}'
    login_host: '{{ item.login_host | default(omit) }}'
    login_password: '{{ item.login_password | default(omit) }}'
    login_unix_socket: '{{ item.login_unix_socket | default(omit) }}'
    login_user: '{{ item.login_user | default(omit) }}'
    name: '{{ item.name | default(omit) }}'
    port: '{{ item.port | default(omit) }}'
    schema: '{{ item.schema | default(omit) }}'
    session_role: '{{ item.session_role | default(omit) }}'
    ssl_mode: '{{ item.ssl_mode | default(omit) }}'
    state: '{{ item.state | default(omit) }}'
    trust_input: '{{ item.trust_input | default(omit) }}'
    version: '{{ item.version | default(omit) }}'
  with_items: '{{ postgresql.extensions | default([]) }}'
  when: postgresql is defined
  register: res
  retries: '{{ postgresql_retries }}'
  delay: '{{ postgresql_delay }}'
  become: true
  no_log: true
